import discord
import json
from discord.ext import commands


def load_config():
    with open("config.json", "r") as f:
        return json.load(f)


config = load_config()
bot = commands.Bot(command_prefix=config["prefix"])


@bot.event
async def on_ready():
    print(f"Logged in as {bot.user.name} ({bot.user.id})")
    print(f"Command prefix: {bot.command_prefix}")
    print(f"{'-' * 10} Client ready {'-' * 10}")
    await bot.change_presence(game=discord.Game(name=config["status"]))


if __name__ == "__main__":
    for extension in config["extensions"]:
        try:
            bot.load_extension(f"extensions.{extension}")
        except Exception as error:
            print(f"Error while loading extension {extension}: [{error}]")

    bot.run(config["token"])
