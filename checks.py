import discord
from discord.ext import commands
from run import config


def get_permissions(ctx):
    permissions = ctx.message.channel.permissions_for(ctx.message.author)
    return permissions


def is_owner(ctx):
    return ctx.message.author.id == config["owner_id"]


def can_manage_messages(ctx):
    permissions = get_permissions(ctx)
    return permissions.manage_messages


def can_manage_nicknames(ctx):
    permissions = get_permissions(ctx)
    return permissions.manage_nicknames


def can_manage_roles(ctx):
    permissions = get_permissions(ctx)
    return permissions.manage_roles
