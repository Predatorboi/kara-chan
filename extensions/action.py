import discord
import json
from discord.ext import commands
from random import choice
from checks import is_owner


async def load_action():
    with open("action.json", "r") as f:
        return json.load(f)


class Action:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, name="action")
    async def action(self, ctx):
        action = await load_action()
        embed = discord.Embed(title="Action statistics")
        for i in action:
            embed.add_field(name=i, value=f"{len(action[i])} gifs", inline=True)
        embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True, name="add")
    @commands.check(is_owner)
    async def action_add(self, ctx, name, url):
        action = await load_action()
        action[name].append(str(url))
        with open("action.json", "w") as f:
            json.dump(action, f)

        embed = discord.Embed(title=f"Gif added to the {name} command")
        embed.set_thumbnail(url=url)
        embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True, name="hug")
    async def hug(self, ctx, member: discord.Member):
        """Sends a hug gif"""
        action = await load_action()
        gifs = action["hug"]
        embed = discord.Embed(description=f"{ctx.message.author.mention} hugged {member.mention}.", color=0xe90fee)
        embed.set_image(url=choice(gifs))
        embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True, name="slap")
    async def slap(self, ctx, member: discord.Member):
        """Sends a slap gif"""
        action = await load_action()
        gifs = action["slap"]
        embed = discord.Embed(description=f"{ctx.message.author.mention} slapped {member.mention}.", color=0xe90fee)
        embed.set_image(url=choice(gifs))
        embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True, name="kiss")
    async def kiss(self, ctx, member: discord.Member):
        """Sends a kiss gif"""
        action = await load_action()
        gifs = action["kiss"]
        embed = discord.Embed(description=f"{ctx.message.author.mention} kissed {member.mention}.", color=0xe90fee)
        embed.set_image(url=choice(gifs))
        embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True, name="cuddle")
    async def cuddle(self, ctx, member: discord.Member):
        """Sends a cuddle gif"""
        action = await load_action()
        gifs = action["cuddle"]
        embed = discord.Embed(description=f"{ctx.message.author.mention} cuddled {member.mention}.", color=0xe90fee)
        embed.set_image(url=choice(gifs))
        embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True, name="lick")
    async def lick(self, ctx, member: discord.Member):
        """Sends a lick gif"""
        action = await load_action()
        gifs = action["lick"]
        embed = discord.Embed(description=f"{ctx.message.author.mention} licked {member.mention}.", color=0xe90fee)
        embed.set_image(url=choice(gifs))
        embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
        await self.bot.say(embed=embed)

# TODO: Pat, poke, lewd, smile, goodnight


def setup(bot):
    bot.add_cog(Action(bot))
