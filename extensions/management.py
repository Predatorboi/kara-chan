import discord
from discord.ext import commands
from checks import is_owner
from run import config


class Management:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(hidden=True)
    @commands.check(is_owner)
    async def load(self, extension):
        """Loads a given extension"""
        try:
            self.bot.load_extension(f"extensions.{extension}")
            await self.bot.say(f"Loaded the **{extension}** extension.")
        except Exception as error:
            await self.bot.say(f"Error while loading extension **{extension}**: [{error}]")

    @commands.command(hidden=True)
    @commands.check(is_owner)
    async def unload(self, extension):
        """Unloads a given extension"""
        if extension == "management":
            await self.bot.say("You cannot unload management, idiot!")
            return
        try:
            self.bot.unload_extension(f"extensions.{extension}")
            await self.bot.say(f"Unloaded the **{extension}** extension.")
        except Exception as error:
            await self.bot.say(f"Error while unloading extension **{extension}**: [{error}]")

    @commands.command(hidden=True)
    @commands.check(is_owner)
    async def reload(self, extension):
        """Reload a given extension"""
        try:
            self.bot.unload_extension(f"extensions.{extension}")
            self.bot.load_extension(f"extensions.{extension}")
            await self.bot.say(f"Reloaded the **{extension}** extension.")
        except Exception as error:
            await self.bot.say(f"Error while reloading extension **{extension}**: [{error}]")

    @commands.command(hidden=True)
    @commands.check(is_owner)
    async def status(self, *title):
        status = ""
        for i in title:
            status += f" {i}"
        config["status"] = status
        await self.bot.change_presence(game=discord.Game(name=str(status)))
        await self.bot.say(f"Changed status to **{str(status)}**")


def setup(bot):
    bot.add_cog(Management(bot))
