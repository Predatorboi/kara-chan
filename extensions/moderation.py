import discord
from discord.ext import commands
from checks import can_manage_messages


class Moderation:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, name="clear")
    @commands.check(can_manage_messages)
    async def clear(self, ctx, amount: int):
        """Deletes a given number of messages"""
        messages = []
        async for message in self.bot.logs_from(ctx.message.channel, limit=int(amount + 1)):
            messages.append(message)
        await self.bot.delete_messages(messages)


def setup(bot):
    bot.add_cog(Moderation(bot))
