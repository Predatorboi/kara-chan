import discord
from discord.ext import commands


class Fun:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="ship")
    async def ship(self, left: discord.Member, right: discord.Member):
        left = left.name[:int(len(left.name) / 1.5)]
        right = right.name[int(len(right.name) / 1.5):]
        await self.bot.say(f"Aww, **{left}{right}** - the cutest couple ever.")


def setup(bot):
    bot.add_cog(Fun(bot))
