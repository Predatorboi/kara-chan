import discord
from discord.ext import commands
from checks import can_manage_messages
from hastebin import post


class Utility:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, name="quote", aliases=["q"])
    async def quote(self, ctx, msg):
        """Quotes a message from a message link
        This will send an embed showing the server, channel, date, author and content of the message from the link"""

        if "https://discordapp.com/channels/" in msg:
            ch = self.bot.get_channel(msg[51:69])
            msg = await self.bot.get_message(ch, msg[70:89])
        elif "https://canary.discordapp.com/channels/" in msg:
            ch = self.bot.get_channel(msg[58:76])
            msg = await self.bot.get_message(ch, msg[77:96])

        embed = discord.Embed(description=f"Message by {msg.author.mention} in **{msg.channel.server.name}**{msg.channel.mention} at {str(msg.timestamp)[:19]}")
        embed.add_field(name="Message content:", value=msg.content)
        embed.set_footer(text=f"Sent by {msg.author.name}", icon_url=msg.author.avatar_url)
        await self.bot.say(embed=embed)
        # await self.bot.delete_message(message)

    @commands.command(pass_context=True, name="avatar")
    async def avatar(self, ctx, member: discord.Member):
        """Shows a given user's avatar"""

        embed = discord.Embed(description=f"{member.mention}'s avatar", color=0xe90fee)
        embed.set_image(url=member.avatar_url.replace("?size=1024", "?size=2048"))
        embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True, name="member")
    async def member(self, ctx, member: discord.Member):
        """Shows given member's info"""
        embed = discord.Embed(title=f"Info about {member.name}")
        embed.set_thumbnail(url=str(member.avatar_url).replace("?size=1024", "?size=2048"))
        embed.add_field(name="Username", value=member.name, inline=True)
        embed.add_field(name="Server nickname", value=member.nick, inline=True)
        embed.add_field(name="Account creation date", value=str(member.created_at)[0:19], inline=True)
        embed.add_field(name="Server join date", value=str(member.joined_at)[0:19], inline=True)
        embed.add_field(name="ID", value=member.id, inline=True)
        embed.add_field(name="Number of roles", value=str(len(member.roles)), inline=True)
        embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True, name="user")
    async def user(self, ctx, *users):
        """Shows given user's info (ID)
        Shows only nicknames if given more than 1 id (space separated)"""

        if len(users) == 1:
            try:
                user = await self.bot.get_user_info(str(users[0]))
            except Exception as e:
                await self.bot.say(f"**{user}** not found: [{e}]")

            embed = discord.Embed(title=f"Info about {user.name}")
            embed.set_thumbnail(url=str(user.avatar_url).replace("?size=1024", "?size=2048"))
            embed.add_field(name="Username", value=user.name, inline=True)
            embed.add_field(name="Account creation date", value=str(user.created_at)[0:19], inline=True)
            embed.add_field(name="ID", value=user.id, inline=True)
            embed.set_footer(text=ctx.message.author.name, icon_url=ctx.message.author.avatar_url)
            await self.bot.say(embed=embed)
        else:
            usernames = []
            for user in users:
                try:
                    user = await self.bot.get_user_info(str(user))
                    usernames.append(user.name)
                except Exception as e:
                    await self.bot.say(f"**{user}** not found: [{e}]")
            await self.bot.say(f"Usernames: {usernames}")

    @commands.command(pass_context=True, name="compare")
    @commands.check(can_manage_messages)
    async def compare(self, ctx, role_a: discord.Role, role_b: discord.Role):
        """Porównuje dwie role."""
        await self.bot.send_typing(ctx.message.channel)
        members_a = []
        members_b = []
        a_no_b = []
        b_no_a = []
        all_members = []
        common_members = []
        for member in ctx.message.server.members:
            if role_a in member.roles:
                members_a.append(member)
            if role_b in member.roles:
                members_b.append(member)

        for member in members_a:
            if member not in all_members:
                all_members.append(member)
        for member in members_b:
            if member not in all_members:
                all_members.append(member)
        for i in all_members:
            if i in members_a and i in members_b:
                common_members.append(i)

            if i in members_a and i not in members_b:
                a_no_b.append(i)

            if i in members_b and i not in members_a:
                b_no_a.append(i)

        out_a = ""
        out_b = ""
        out_b_no_a = ""
        out_a_no_b = ""
        out_all = ""
        out_common = ""
        for member in members_a:
            out_a += f"{member.name}\n"
        for member in members_b:
            out_b += f"{member.name}\n"
        for member in a_no_b:
            out_a_no_b += f"{member.name}\n"
        for member in b_no_a:
            out_b_no_a += f"{member.name}\n"
        for member in all_members:
            out_all += f"{member.name}\n"
        for member in common_members:
            out_common += f"{member.name}\n"

        out = ""
        out += f"W roli **{role_a.name}** znajduje się {len(members_a)} osób: {post(out_a.encode('utf-8'))}\n"
        out += f"W roli **{role_b.name}** znajduje się {len(members_b)} osób: {post(out_b.encode('utf-8'))}\n"
        out += f"Obydwie role dzieli {len(common_members)} osób: {post(out_common.encode('utf-8'))}\n"
        out += f"Tylko w roli **{role_a.name}** znajduje się {len(a_no_b)} osób: {post(out_a_no_b.encode('utf-8'))}\n"
        out += f"Tylko w roli **{role_b.name}** znajduje się {len(b_no_a)} osób: {post(out_b_no_a.encode('utf-8'))}\n"
        out += f"W obydwu rolach znajduje się łącznie {len(all_members)} osób: {post(out_all.encode('utf-8'))}"
        await self.bot.say(out)

    @commands.command(pass_context=True, name="msg")
    @commands.check(can_manage_messages)
    async def msg(self, ctx, channel: discord.Channel):
        await self.bot.send_message(channel, ctx.message.content[27:])


def setup(bot):
    bot.add_cog(Utility(bot))
