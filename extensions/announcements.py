import discord
from discord.ext import commands
from checks import is_owner


class Announcements:
    def __init__(self, bot):
        self.bot = bot

    async def on_member_join(self, member):
        embed = discord.Embed(description=f"Welcome to **{member.server.name}**, {member.mention}")
        embed.set_thumbnail(url=str(member.avatar_url).replace("?size=1024", "?size=2048"))

        if member.server.id == "525428727178592276":
            await self.bot.send_message(discord.Object("525774152552939531"), embed=embed)
        elif member.server.id == "342022299957854220":
            await self.bot.send_message(discord.Object("483273472555089930"), embed=embed)

    async def on_member_remove(self, member):
        embed = discord.Embed(description=f"{member.mention} has left the server.")
        embed.set_thumbnail(url=str(member.avatar_url).replace("?size=1024", "?size=2048"))

        if member.server.id == "525428727178592276":
            await self.bot.send_message(discord.Object("525774152552939531"), embed=embed)
        elif member.server.id == "342022299957854220":
            await self.bot.send_message(discord.Object("483273472555089930"), embed=embed)

# ---------------------------DEBUG-------------------------
    @commands.command(hidden=True)
    @commands.check(is_owner)
    async def def_ch(self, member: discord.Member):
        await self.bot.say(f"[{type(member.server.default_channel)}] {member.server.default_channel} \n [{type(member.server)}] {member.server} \n [{type(member)}] {member}")

        embed = discord.Embed(title=f"{member.name} has joined the server.")
        embed.set_thumbnail(url=str(member.avatar_url).replace("?size=1024", "?size=2048"))

        if member.server.id == "525428727178592276":
            await self.bot.send_message(discord.Object("525774152552939531"), embed=embed)
        elif member.server.id == "342022299957854220":
            await self.bot.send_message(discord.Object("483273472555089930"), embed=embed)

    @commands.command(hidden=True)
    @commands.check(is_owner)
    async def ch(self, channel: discord.Channel, member: discord.Member):
        embed = discord.Embed(title=f"{member.name} has joined the server.", description=f"Welcome to **{member.server.name}**")
        # embed.add_field(name="Welcome to", value=member.server.name)
        embed.set_thumbnail(url=str(member.avatar_url).replace("?size=1024", "?size=2048"))
        await self.bot.send_message(channel, embed=embed)


def setup(bot):
    bot.add_cog(Announcements(bot))
